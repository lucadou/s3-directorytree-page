'use strict';
function getFileIcon(fileName, htmlTag = "i", paddingClass = "pr-1") {
  /* fileName - the name of the file, e.g. "file.txt".
   *  Note: in case of complex extensions (e.g. file.tar.gz), only the final
   *  extension (gz) will be used.
   * htmlTag - the tag you want the element to use, e.g. "span", "div" (defaults to "i").
   * paddingClass - the class to use for padding the icon; defaults to Bootstrap's "pr-1". (Can also be used to add additional classes.)
   */
  let fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
    /* If there is no extension, substring(-1) returns the whole string.
     * As a side effect, if someone uploads a file called "docx", the
     * extension will be assumed as "docx", but this is fine in my opinion.
     */
  switch(fileExtension.toLowerCase()) {
    // Document file extensions
    case 'doc':
    case 'docx':
    case 'odt':
      return '<{0} class="fas fa-file-word {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Spreadsheet file extensions
    case 'xls':
    case 'xlsx':
    case 'ods':
    case 'csv':
      return '<{0} class="fas fa-file-excel {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Presentation file extensions
    case 'ppt':
    case 'pptx':
    case 'odp':
      return '<{0} class="fas fa-file-powerpoint {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // PDF file extension
    case 'pdf':
      return '<{0} class="fas fa-file-pdf {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Image file extensions
    case 'bmp':
    case 'gif':
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'webp':
      return '<{0} class="fas fa-file-image {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Audio file extensions
    case 'aac':
    case 'flac':
    case 'm4a':
    case 'mp3':
    case 'ogg':
    case 'wav':
    case 'wma':
    case 'wave':
      return '<{0} class="fas fa-file-audio {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Video file extensions
    case 'avi':
    case 'flv':
    case 'm4v':
    case 'mkv':
    case 'mov':
    case 'mp4':
    case 'mpg':
    case 'mpeg':
    case 'mpv':
    case 'mp2':
    case 'mts':
    case 'm2ts':
    case 'ts':
    case 'ogv':
    case 'vob':
    case 'webm':
    case 'wmv':
      return '<{0} class="fas fa-file-video {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Archive file extensions
    case '7z':
    case 'apk':
    case 'ark':
    case 'bz2':
    case 'cab':
    case 'dmg':
    case 'gz':
    case 'jar':
    case 'lz':
    case 'lzma':
    case 'pea':
    case 'rar':
    case 'tar':
    case 'tgz':
    case 'txz':
    case 'tz':
    case 'xz':
    case 'zip':
      return '<{0} class="fas fa-file-archive {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Disk image (CD-oriented) file extensions
    case 'bin':
    case 'cue':
    case 'img':
    case 'iso':
    case 'mdf':
    case 'mds':
      return '<{0} class="fas fa-compact-disc {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Disk image (HDD-oriented) file extensions
    case 'qcow':
    case 'vdi':
    case 'vhd':
    case 'vmdk':
      return '<{0} class="fas fa-hdd {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Disk image (FDD-oriented) file extensions
    case 'vfd':
      return '<{0} class="fas fa-save {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Text file extensions
    case 'log':
    case 'text':
    case 'txt':
      return '<{0} class="fas fa-file-alt {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Code file extensions
    case 'asm':
    case 'asp':
    case 'aspx':
    case 'bash':
    case 'bat':
    case 'bsh':
    case 'c':
    case 'cmd':
    case 'cpp':
    case 'cs':
    case 'css':
    case 'diff':
    case 'erb':
    case 'f':
    case 'h':
    case 'hpp':
    case 'hs':
    case 'htm':
    case 'html':
    case 'inf':
    case 'ini':
    case 'java':
    case 'js':
    case 'json':
    case 'jsp':
    case 'lisp':
    case 'lsp':
    case 'lua':
    case 'nfo':
    case 'nsh':
    case 'pas':
    case 'patch':
    case 'php':
    case 'pl':
    case 'ps1':
    case 'py':
    case 'r':
    case 'rb':
    case 'ruby':
    case 'sh':
    case 'shtm':
    case 'shtml':
    case 'sql':
    case 'tex':
    case 'vb':
    case 'vbs':
    case 'xht':
    case 'xhtml':
    case 'xml':
    case 'xul':
    case 'yaml':
    case 'yml':
    case 'zsh':
      return '<{0} class="fas fa-file-code {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
    // Everything else
    default:
      return '<{0} class="fas fa-file {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
  }
}