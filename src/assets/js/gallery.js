'use strict';
let galleryModal = null;
let galleryModalTitle = null;
let galleryModalBody = null;
let galleryBtnPrev = null;
let galleryBtnNext = null;

function setPrevImage(prevElement) {
  if (prevElement !== null) {
    galleryBtnPrev.text('Previous Image ({0})'.split('{0}').join(prevElement.text.trim()));
      // .trim() removes surrounding whitespace
    galleryBtnPrev.removeClass('disabled');
    galleryBtnPrev.attr('disabled', null);
    galleryBtnPrev.attr('data-source', prevElement.id);
  } else {
    galleryBtnPrev.text('Previous Image');
    galleryBtnPrev.addClass('disabled');
    galleryBtnPrev.attr('disabled', true);
  }
}

function setNextImage(nextElement) {
  if (nextElement !== null) {
    galleryBtnNext.text('Next Image ({0})'.split('{0}').join(nextElement.text.trim()));
      // .trim() removes surrounding whitespace
    galleryBtnNext.removeClass('disabled');
    galleryBtnNext.attr('disabled', null);
    galleryBtnNext.attr('data-source', nextElement.id);
  } else {
    galleryBtnNext.text('Next Image');
    galleryBtnNext.addClass('disabled');
    galleryBtnNext.attr('disabled', true);
  }
}

function galleryChangeImage(imageElement) {
  imageElement = $(imageElement);
  galleryModalTitle.text('Image Gallery - {}'.split('{}').join(imageElement.text().trim()));
  galleryModalBody.html('<img class="img-fluid center" src="{}" data-source="{0}">'.split('{}').join(imageElement.attr('href')).split('{0}').join(imageElement.attr('id')));
  
  let parentElement = imageElement.parent();
  let prevElement = null;
  let nextElement = null;

  // Set the previous element
  let parentPrev = parentElement.prev();
  while (parentPrev !== null && parentPrev.length !== 0 && prevElement === null) {
    if (parentPrev.children('.accordion-row-toggler').length === 1) {
      prevElement = parentPrev.children('a:not(.accordion-row-toggler)')[0];
        // Guaranteed to be in every file listing, image or not
    } else if (parentPrev.children('.accordion-row-toggler').length > 1) {
      console.log('Error: >1 image embed found while searching for previous image!');
    } /*else { // < 1
      // The previous element is not an image so it cannot be displayed in the gallery
    }*/
    parentPrev = parentPrev.prev();
  }

  // Set the next element
  let parentNext = parentElement.next();
  while (parentNext !== null && parentNext.length !== 0 && nextElement === null) {
    if (parentNext.children('.accordion-row-toggler').length === 1) {
      nextElement = parentNext.children('a:not(.accordion-row-toggler)')[0];
        // Guaranteed to be in every file listing, image or not
    } else if (parentNext.children('.accordion-row-toggler').length > 1) {
      console.log('Error: >1 image embed found while searching for next image!');
    } /*else { // < 1
      // The next element is not an image so it cannot be displayed in the gallery
    }*/
    parentNext = parentNext.next();
  }

  // Setup Previous button
  setPrevImage(prevElement);

  // Setup Next button
  setNextImage(nextElement);
}

function galleryPrevImage() {
  let newID = galleryBtnPrev.attr('data-source');
  if (newID !== null && newID !== undefined && newID !== $(galleryModalBody.children('img')[0]).attr('data-source')) {
    galleryChangeImage($('#{}'.split('{}').join(newID)));
  }
}

function galleryNextImage() {
  let newID = galleryBtnNext.attr('data-source');
  if (newID !== null && newID !== undefined && newID !== $(galleryModalBody.children('img')[0]).attr('data-source')) {
    galleryChangeImage($('#{}'.split('{}').join(newID)));
  }
}

function galleryOpen(callingElement) {
  galleryChangeImage(callingElement.parent().children('a:not(.accordion-row-toggler)')[0]);
}

function galleryInitialSetup() {
  galleryModal = $('#galleryModal');
  galleryModalTitle = $('#galleryModalLabel');
  galleryModalBody = $('#galleryModalBody');

  galleryBtnPrev = $('#galleryModalPrev');
  galleryBtnNext = $('#galleryModalNext');

  galleryBtnPrev.on('click', galleryPrevImage);
  galleryBtnNext.on('click', galleryNextImage);

  // Setup event handlers
  $(document).on('click', '.button-open-gallery', function() {
    /* This will bind the click event to any new button.button-open-gallery
     * elements added after this method executes.
     * Credit for this goes to: https://stackoverflow.com/a/11961219
     * More info: https://stackoverflow.com/a/1207393
     */
    galleryOpen($(this), galleryBtnPrev, galleryBtnNext);
  });

  // Setup keyboard shortcuts
  $(document).keydown(function(e) {
    let scrollAmount = 45; // 45 is a nice, natural-seeming value from my testing
    switch(e.key) {
      case 'ArrowLeft':
        if(galleryModal[0].classList.contains('show')) {
          galleryPrevImage();
        }
        break;
      case 'ArrowRight':
        if(galleryModal[0].classList.contains('show')) {
          galleryNextImage();
        }
        break;
      case 'ArrowUp':
        galleryModalBody.get(0).scrollTop -= scrollAmount;
        // I learned about scrollTop from https://stackoverflow.com/a/635735
        // .get(0) to get the underling DOM element so I can use .scrollTop
        // https://learn.jquery.com/using-jquery-core/faq/how-do-i-pull-a-native-dom-element-from-a-jquery-object/
        break;
      case 'ArrowDown':
        galleryModalBody.get(0).scrollTop += scrollAmount;
        break;
      default:
        return;
    }
    e.preventDefault();
    // https://stackoverflow.com/a/6011119
  });

  // Clean up modal after closing
  galleryModal.on('hidden.bs.modal', function(e) {
    galleryModalTitle.text('Image Gallery - Waiting...');
    galleryModalBody.text('No image specified');
    galleryBtnPrev.text('Previous Image');
    galleryBtnPrev.attr('data-source', null);
    galleryBtnPrev.attr('disabled', null);
    galleryBtnPrev.removeClass('disabled');
    galleryBtnNext.text('Previous Image');
    galleryBtnNext.attr('data-source', null);
    galleryBtnNext.attr('disabled', null);
    galleryBtnNext.removeClass('disabled');
  });
}